const form = document.querySelector('form');

function validateEmail(email) {
    const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return regex.test(String(email).toLowerCase());
}

form.addEventListener('submit', (event) => {
    event.preventDefault();

    const input = document.getElementById('email');
    const email = input.value;
    const nodeError = document.getElementById('error');

    if (email.trim() === '') {
        nodeError.innerText = 'You must enter an email first';
        input.classList.add('error-input')
    } else if (!validateEmail(email.trim())) {
        nodeError.innerText = 'You must enter a valid email';
        input.classList.add('error-input');
    } 
    else {
        nodeError.innerText = '';
        input.classList.remove('error-input');
    }

});